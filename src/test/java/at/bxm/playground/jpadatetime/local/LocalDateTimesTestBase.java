package at.bxm.playground.jpadatetime.local;

import static java.time.Month.DECEMBER;
import static java.time.Month.JANUARY;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import at.bxm.playground.jpadatetime.JpaTestBase;

abstract class LocalDateTimesTestBase extends JpaTestBase
{
    @Autowired
    private LocalDateTimesRepository localDateTimesRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Tag("timestamp")
    @Tag("read")
    void readEntityTimestamp()
    {
        var entity = localDateTimesRepository.getById("init");
        assertThat(entity.getTimestamp()).isEqualTo(LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0));
    }

    @Test
    @Tag("timestamptz")
    @Tag("read")
    void readEntityTimestampWithTimezone()
    {
        var entity = localDateTimesRepository.getById("init");
        assertThat(entity.getTimestamptz()).isEqualTo(LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0));
    }

    @Test
    @Tag("timestamp")
    @Tag("write")
    void writeEntityTimestamp()
    {
        localDateTimesRepository.save(LocalDateTimes.builder().id("test")
            .timestamp(LocalDateTime.of(2022, JANUARY, 1, 12, 0)).build());
        localDateTimesRepository.flush();
        jdbcTemplate.query("select timestamp from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamp")).isEqualTo("2022-01-01 12:00:00");
        });
    }

    @Test
    @Tag("timestamptz")
    @Tag("write")
    void writeEntityTimestampWithTimezone()
    {
        localDateTimesRepository.save(LocalDateTimes.builder().id("test")
            .timestamptz(LocalDateTime.of(2022, JANUARY, 1, 12, 0)).build());
        localDateTimesRepository.flush();
        jdbcTemplate.query("select timestamptz from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamptz")).isEqualTo("2022-01-01 12:00:00");
        });
    }
}
