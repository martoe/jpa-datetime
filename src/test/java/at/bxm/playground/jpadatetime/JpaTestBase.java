package at.bxm.playground.jpadatetime;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = NONE)
@ContextConfiguration(classes = JpaConfiguration.class)
public abstract class JpaTestBase
{
}
