package at.bxm.playground.jpadatetime.legacy;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("europe")
class LocalDateTimesEuropeTest extends LegacyDatesTestBase
{
}
