package at.bxm.playground.jpadatetime.legacy;

import static java.time.Month.DECEMBER;
import static java.time.Month.JANUARY;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import at.bxm.playground.jpadatetime.JpaTestBase;

abstract class LegacyDatesTestBase extends JpaTestBase
{
    @Autowired
    private LegacyDatesRepository legacyDatesRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Tag("timestamp")
    @Tag("read")
    void readEntityTimestamp()
    {
        var entity = legacyDatesRepository.getById("init");
        assertThat(entity.getTimestamp()).isEqualTo(
            Timestamp.from(LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0).atOffset(UTC).toInstant()));
    }

    @Test
    @Tag("timestamptz")
    @Tag("read")
    void readEntityTimestampWithTimezone()
    {
        var entity = legacyDatesRepository.getById("init");
        assertThat(entity.getTimestamptz()).isEqualTo(
            Timestamp.from(LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0).atOffset(UTC).toInstant()));
    }

    @Test
    @Tag("timestamp")
    @Tag("write")
    void writeEntityTimestamp()
    {
        legacyDatesRepository.save(LegacyDates.builder().id("test")
            .timestamp(Date.from(LocalDateTime.of(2022, JANUARY, 1, 12, 0).atOffset(UTC).toInstant())).build());
        legacyDatesRepository.flush();
        jdbcTemplate.query("select timestamp from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamp")).isEqualTo("2022-01-01 12:00:00");
        });
    }

    @Test
    @Tag("timestamptz")
    @Tag("write")
    void writeEntityTimestampWithTimezone()
    {
        legacyDatesRepository.save(LegacyDates.builder().id("test")
            .timestamptz(Date.from(LocalDateTime.of(2022, JANUARY, 1, 12, 0).atOffset(UTC).toInstant())).build());
        legacyDatesRepository.flush();
        jdbcTemplate.query("select timestamptz from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamptz")).isEqualTo("2022-01-01 12:00:00");
        });
    }
}
