package at.bxm.playground.jpadatetime.offset;

import static java.time.Month.DECEMBER;
import static java.time.Month.JANUARY;
import static java.time.ZoneOffset.UTC;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import at.bxm.playground.jpadatetime.JpaTestBase;

abstract class OffsetDateTimesTestBase extends JpaTestBase
{
    @Autowired
    private OffsetDateTimesRepository offsetDateTimesRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Tag("timestamp")
    @Tag("read")
    void readEntityTimestamp()
    {
        var entity = offsetDateTimesRepository.getById("init");
        assertThat(entity.getTimestamp()).isEqualTo(
            LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0).atOffset(UTC));
    }

    @Test
    @Tag("timestamptz")
    @Tag("read")
    void readEntityTimestampWithTimezone()
    {
        var entity = offsetDateTimesRepository.getById("init");
        assertThat(entity.getTimestamptz()).isEqualTo(
            LocalDateTime.of(2000, DECEMBER, 31, 12, 0, 0).atOffset(UTC));
    }

    @Test
    @Tag("timestamp")
    @Tag("write")
    void writeEntityTimestamp()
    {
        offsetDateTimesRepository.save(OffsetDateTimes.builder().id("test")
            .timestamp(OffsetDateTime.from(LocalDateTime.of(2022, JANUARY, 1, 12, 0, 0).atOffset(UTC))).build());
        offsetDateTimesRepository.flush();
        jdbcTemplate.query("select timestamp from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamp")).isEqualTo("2022-01-01 12:00:00");
        });
    }

    @Test
    @Tag("timestamptz")
    @Tag("write")
    void writeEntityTimestampWithTimezone()
    {
        offsetDateTimesRepository.save(OffsetDateTimes.builder().id("test")
            .timestamptz(OffsetDateTime.from(LocalDateTime.of(2022, JANUARY, 1, 12, 0, 0).atOffset(UTC))).build());
        offsetDateTimesRepository.flush();
        jdbcTemplate.query("select timestamptz from datetime where id='test'", rs ->
        {
            assertThat(rs.getString("timestamptz")).isEqualTo("2022-01-01 13:00:00+01");
        });
    }
}
