package at.bxm.playground.jpadatetime.offset;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("utc")
class OffsetDateTimesUtcTest extends OffsetDateTimesTestBase
{
}
