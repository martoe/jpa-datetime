package at.bxm.playground.jpadatetime.legacy;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "datetime")
public class LegacyDates
{
    @Id
    private String id;
    private Date timestamp;
    private Date timestamptz;
}
