package at.bxm.playground.jpadatetime.legacy;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LegacyDatesRepository extends JpaRepository<LegacyDates, String>
{
}
