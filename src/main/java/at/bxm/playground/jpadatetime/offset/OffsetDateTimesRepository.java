package at.bxm.playground.jpadatetime.offset;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OffsetDateTimesRepository extends JpaRepository<OffsetDateTimes, String>
{
}
