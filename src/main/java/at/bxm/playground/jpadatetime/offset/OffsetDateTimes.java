package at.bxm.playground.jpadatetime.offset;

import java.time.OffsetDateTime;
import java.time.OffsetTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "datetime")
public class OffsetDateTimes
{
    @Id
    private String id;
    private OffsetDateTime date;
    private OffsetTime time;
    private OffsetTime timetz;
    private OffsetDateTime timestamp;
    private OffsetDateTime timestamptz;
}
