package at.bxm.playground.jpadatetime;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackageClasses = JpaConfiguration.class)
@EntityScan(basePackageClasses = JpaConfiguration.class)
public class JpaConfiguration
{
}
