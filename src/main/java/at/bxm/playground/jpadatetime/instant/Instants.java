package at.bxm.playground.jpadatetime.instant;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "datetime")
public class Instants
{
    @Id
    private String id;
    private Instant timestamp;
    private Instant timestamptz;
}
