package at.bxm.playground.jpadatetime.instant;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InstantsRepository extends JpaRepository<Instants, String>
{
}
