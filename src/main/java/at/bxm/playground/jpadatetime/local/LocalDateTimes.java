package at.bxm.playground.jpadatetime.local;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "datetime")
public class LocalDateTimes
{
    @Id
    private String id;
    private LocalDate date;
    private LocalTime time;
    private LocalTime timetz;
    private LocalDateTime timestamp;
    private LocalDateTime timestamptz;
}
