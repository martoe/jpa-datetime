package at.bxm.playground.jpadatetime.local;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalDateTimesRepository extends JpaRepository<LocalDateTimes, String>
{
}
