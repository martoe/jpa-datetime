Timezones with java.time and JPA/Hibernate
==========================================

Test setup
----------
* Postgres database servers with different timezones - `UTC` vs. `Europe/Amsterdam` (local) vs. `America/Aruba`
```
docker-compose up -d
docker exec jpa-datetime-pg-default-1 date --rfc-3339=seconds
docker exec jpa-datetime-pg-europe-1 date --rfc-3339=seconds
docker exec jpa-datetime-pg-america-1 date --rfc-3339=seconds
```
* Different database field types - `timestamp` vs. `timestamptz`
* Different mapped Java types - `LocalDateTime` vs. `OffsetDateTime` vs. `Instant` vs. legacy `Date`

Test cases
----------
* "read":  Writing a 12:00 timestamp with serverside SQL and reading it with JPA
* "write": Writing a 12:00 timestamp with JPA and reading it with JDBC (as string)

Test results
------------
(incorrect results are marked "*")
(note: the "Date read" value refers to the local timezone, so 13:00 is the expected result)

### timestamp without timezone:

|              | LocalDateTime read | LocalDateTime write | OffsetDateTime read | OffsetDateTime write | Instant read | Instant write | Date read | Date write |
|--------------|--------------------|---------------------|---------------------|----------------------|--------------|---------------|-----------|------------|
| UTC          | 12:00              | 12:00               | 12:00+01:00 *       | 13:00 *              | 11:00Z *     | 13:00 *       | 12:00 *   | 13:00 *    |
| local/Europe | 12:00              | 12:00               | 12:00+01:00 *       | 13:00 *              | 11:00Z *     | 13:00 *       | 12:00 *   | 13:00 *    |
| America      | 12:00              | 12:00               | 12:00+01:00 *       | 13:00 *              | 11:00Z *     | 13:00 *       | 12:00 *   | 13:00 *    |

...and with `spring.jpa.properties.hibernate.jdbc.time_zone=UTC`:

| timezone     | LocalDateTime read | LocalDateTime write | OffsetDateTime read | OffsetDateTime write | Instant read | Instant write | Date read | Date write |
|--------------|--------------------|---------------------|---------------------|----------------------|--------------|---------------|-----------|------------|
| UTC          | 13:00 *            | 11:00 *             | 13:00+01:00         | 12:00                | 12:00Z       | 12:00         | 13:00     | 12:00      |
| local/Europe | 13:00 *            | 11:00 *             | 13:00+01:00         | 12:00                | 12:00Z       | 12:00         | 13:00     | 12:00      |
| America      | 13:00 *            | 11:00 *             | 13:00+01:00         | 12:00                | 12:00Z       | 12:00         | 13:00     | 12:00      |

### timestamp with timezone:

|              | LocalDateTime read | LocalDateTime write | OffsetDateTime read | OffsetDateTime write | Instant read | Instant write | Date read  | Date write |
|--------------|--------------------|---------------------|---------------------|----------------------|--------------|---------------|------------|------------|
| UTC          | 13:00 *            | 12:00+01 *          | 13:00+01:00         | 13:00+01             | 12:00Z       | 13:00+01      | 13:00      | 13:00+01 * |
| local/Europe | 13:00 *            | 12:00+01 *          | 13:00+01:00         | 13:00+01             | 12:00Z       | 13:00+01      | 13:00      | 13:00+01 * |
| America      | 13:00 *            | 12:00+01 *          | 13:00+01:00         | 13:00+01             | 12:00Z       | 13:00+01      | 13:00      | 13:00+01 * |

Takeaways for timestamps without timezone
-----------------------------------------
* The timezone of the database server doesn't matter
* Using java.time.LocalDateTime is ok from a database perspective, however the application must deal with timezones then
  (trouble ahead if there are multiple clients with different JVM timezones)
* When using java.time.OffsetDateTime or java.time.Instant, `jdbc.time_zone=UTC` must be set 
  (to make the database values independent of the JVM timezone)
* java.util.Date/java.sql.Timestamp behaves like java.time.Instant

Takeaways for timestamps with timezone
-----------------------------------------
* The timezone of the database server matter only when using serverside SQL without specifying the timezone
* Writing and reading with timezone information (java.time.OffsetDateTime, java.time.Instant) works as expected
* When writing and reading without timezone information (java.time.LocalDateTime), the local JVM timezone is used,
  which leads to wrong values!
* `jdbc.time_zone=UTC` has no effect

The recommended approach (simple yet correct)
---------------------------------------------
* Every database timestamp shall be regarded as UTC value
* At database level, use timestamps without timezone
* At application level, set `jdbc.time_zone=UTC` and use java.time.Instant
