create schema test;
create table test.datetime
(
    id          varchar(100) not null,
    date        date,
    time        time,
    timetz      timetz,
    timestamp   timestamp,
    timestamptz timestamptz,
    primary key (id)
);
insert into test.datetime(id, date, time, timetz, timestamp, timestamptz)
values ('init', '2000-12-31', '12:00:00', '12:00:00', '2000-12-31 12:00:00', '2000-12-31 12:00:00+00');
